<?php





/**
 * Implementation of hook_view
 */
function class_view(&$node, $teaser = FALSE, $page = FALSE)
{
	$node = node_prepare($node, $teaser);
	if(isset($node->grade_level)) {
		$node->body .= '<p><strong>Grade Level: ' . 
			class_t_grade_level($node->grade_level) . 
			'</strong></p>';
	}
	$node->body .=
		'<p class="course-instructions">' .
		$node->chalkboard .
		'</p>';
}


/**
 * Implementation of hook_views_tables
 */
function class_views_tables()
{
	$tables['class_node'] = array(
		'name' => 'class',
		'join' => array(
			'left' => array(
				'table' => 'og_node',
				'field' => 'gid',
			),
			'right' => array(
				'field' => 'nid',
			),
			'extra' => array(
				'is_active' => 1,
			),
		),
		'filters' => array(
			'currentteacher' => array(
				'field' => 'nid',
				'name' => t('CLASS: Post is in a class'),
				'handler' => 'class_views_handler',
				'operator' => array('=' => t('Is Equal To')),
				'list' => array('true' => t('True')),
				'list-type' => 'select',
				'help' => t('Posts are filtered so that only posts in classes show up.')
			)
		)
	);
	
	$tables['class_group'] = array(
		'name' => 'class',
		'join' => array(
			'left' => array(
				'table' => 'og',
				'field' => 'nid'
			),
			'right' => array(
				'field' => 'nid'
			),
		),
		'fields' => array(
			'enrollment' => array(
				'name' => t('CLASS: Maximum Enrollment'),
				'handler' => 'class_views_handler_field_enrollment',
				'sortable' => true,
				'help' => t('This will display the maximum enrollment of a class.')
			),
			'grade_level' => array(
				'name' => t('CLASS: Grade Level'),
				'sortable' => true,
				'handler' => 'class_views_handler_field_gradelevel',
				'help' => t('This will display the grade level of a class.')
			),
		),
	);
	return $tables;
}



function class_views_handler($op, $filter, $filterinfo, &$query) 
{
	// inner join the class table to filter
	$tablename = 'og_node';
	$query->ensure_table('og_node');
	$query->add_field('gid', 'og_node');
	$query->add_table('class', false, 1, array( 'type' => 'inner', 'left' => array( 'table' => $tablename, 'field' => 'gid' ), 'right' => array('field' => 'nid')));

}

function class_views_handler_field_enrollment($fieldinfo, $fielddata, $value, $data)
{
	if($value) 
	{
		return theme('class_max_enrollment', $value);
	}
	else
	{
		return theme('class_max_enrollment', t('unlimited'));
	}
}

function theme_class_max_enrollment($value)
{
	return $value;
}


function class_views_handler_field_gradelevel($fieldinfo, $fielddata, $value, $data)
{
	return class_t_grade_level($value);
}











/**
 * Implementation of hook_views_default_views
 */
function class_views_default_views()
{
//-------------------------------------------------------------------------------------------------------
  $view = new stdClass();
  $view->name = 'classes_to_take';
  $view->description = 'A listing of the classes available for enrollment.';
  $view->access = array (
  0 => '2',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Classes to Take';
  $view->page_header = "<p><strong>Listed here are the classes that are available to take.</strong></p>\r\n<p><?php echo l(t('Classes to Take'), 'classes_to_take', array('class' => 'button')) ?>  <?php echo l(t('Classes to Copy'), 'classes_to_copy', array('class' => 'button')) ?></p>";
  $view->page_header_format = '2';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'There are not currently any classes available.';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'classes_to_take';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->menu = TRUE;
  $view->menu_title = 'classes to take';
  $view->menu_tab = FALSE;
  $view->menu_tab_default = FALSE;
  $view->menu_weight = '';
  $view->sort = array (
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'class_misc_predefined',
      'field' => 'nid',
      'label' => 'TWB Approved',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Class',
      'handler' => 'views_handler_field_nodelink',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'og',
      'field' => 'description',
      'label' => 'Description',
    ),
    array (
      'tablename' => 'class_group',
      'field' => 'enrollment',
      'label' => '# Spaces',
    ),
    array (
      'tablename' => 'og',
      'field' => 'count',
      'label' => '# Enrolled',
    ),
    array (
      'tablename' => 'users',
      'field' => 'name',
      'label' => 'Instructor',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'og',
      'field' => 'subscribe',
      'label' => 'Subscribe',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'class',
),
    ),
    array (
      'tablename' => 'twb_copy',
      'field' => 'copyable',
      'operator' => '=',
      'options' => '',
      'value' => 'not copyable',
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(class_misc_predefined, node, og, class_group, users, twb_copy);
  $views[$view->name] = $view;

  
  $view=drupal_clone($view);
  unset($view->field[0]);
  $view->filter[] = array (
      'tablename' => 'class_misc_predefined',
      'field' => 'predefined',
      'operator' => '=',
      'options' => '',
      'value' => 'true',
    );
  $view->name = 'approved_classes';
  $view->page_title = 'approved classes';
  $view->menu_title = 'approved classes';
  $view->page_empty = 'There are no approved classes available.';
  $view->url = 'approved_classes';
  $views[$view->name] = $view;
  
  $view = new stdClass();
  $view->name = 'my_classes';
  $view->description = 'A listing of the classes a user is enrolled in.';
  $view->access = array (
  0 => '2',
  1 => '4',
  2 => '5',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'My Classes';
  $view->page_header = 'Listed here are the classes you are enrolled in.  Click the class name to see that class\'s syllabus.';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'You are not currently enrolled in any classes.';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'class/dummyview';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->menu = false;
  $view->menu_title = 'classes';
  $view->menu_tab = TRUE;
  $view->menu_tab_default = FALSE;
  $view->menu_weight = '';
  $view->sort = array (
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Class Name',
      'handler' => 'class_views_handler_field_node',
    ),
    array (
      'tablename' => 'og',
      'field' => 'description',
      'label' => 'Description',
    ),
        array (
      'tablename' => 'class_group',
      'field' => 'enrollment',
      'label' => '# Spaces',
    ),
    array (
      'tablename' => 'og',
      'field' => 'count',
      'label' => '# Enrolled',
    ),
    array (
      'tablename' => 'users',
      'field' => 'name',
      'label' => 'Instructor',
      'sortable' => '1',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'og_uid',
      'field' => 'currentuidsimple',
      'operator' => '=',
      'options' => '',
      'value' => '***CURRENT_USER***',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'class',
),
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node, og, users, og_uid);
  $views[$view->name] = $view;

  $view = new stdClass();
  $view->name = 'my_workspace';
  $view->description = 'Shows new activity on the system that is related to the current user\'s classes.';
  $view->access = array (
  0 => '2',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Class recent activity';
  $view->page_header = 'See posts in your classes.';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'No posts in any of your classes.';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'class/dummmyview';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '25';
  $view->menu = false;
  $view->menu_title = 'all';
  $view->menu_tab = TRUE;
  $view->menu_tab_default = TRUE;
  $view->menu_weight = '';
  $view->sort = array (
    array (
      'tablename' => 'node_comment_statistics',
      'field' => 'last_comment_timestamp',
      'sortorder' => 'DESC',
      'options' => '',
    ),
  );
  $view->argument = array (
    array (
      'type' => 'gid',
      'argdefault' => '2',
      'title' => '%1',
      'options' => '',
      'wildcard' => 'any',
      'wildcard_substitution' => 'all classes',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'label' => 'Type',
    ),
    array (
      'tablename' => 'og_node_data',
      'field' => 'title',
      'label' => 'Class',
    ),
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Title',
      'handler' => 'views_handler_field_nodelink_with_mark',
    ),
    array (
      'tablename' => 'users',
      'field' => 'name',
      'label' => 'Author',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'label' => 'Modified',
      'handler' => 'views_handler_field_date_small',
    ),
    array (
      'tablename' => 'node_comment_statistics',
      'field' => 'comment_count',
      'label' => 'Replies',
      'handler' => 'views_handler_comments_with_new',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
    array (
      'tablename' => 'og_uid_node',
      'field' => 'currentuid',
      'operator' => '=',
      'options' => '',
      'value' => '***CURRENT_USER***',
    ),
    array (
      'tablename' => 'class_node',
      'field' => 'currentteacher',
      'operator' => '=',
      'options' => '',
      'value' => 'true',
    ),
        array (
      'tablename' => 'node',
      'field' => 'status',
      'operator' => '=',
      'options' => '',
      'value' => '1',
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node_comment_statistics, node, og_node_data, users, og_uid_node, class_node);
  $views[$view->name] = $view;

  $view = new stdClass();
  $view->name = 'classes_to_copy';
  $view->description = 'A listing of the classes available for copying.';
  $view->access = array (
  0 => '2',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Classes to Copy';
  $view->page_header = "<p><strong>Listed here are the classes that are available to copy.</strong></p>\r\n<p><a href='classes_to_take' class='button'>Classes to Take</a>  <a href='classes_to_copy' class='button'>Classes to Copy</a></p>";
  $view->page_header_format = '3';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'There are not currently any classes available.';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'classes_to_copy';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->menu = TRUE;
  $view->menu_title = 'classes to copy';
  $view->menu_tab = FALSE;
  $view->menu_tab_default = FALSE;
  $view->menu_weight = '';
  $view->sort = array (
  );
  $view->argument = array (
  );
  $view->field = array (
    array (
      'tablename' => 'class_misc_predefined',
      'field' => 'nid',
      'label' => 'TWB Approved',
      'sortable' => '1',
      'defaultsort' => 'ASC',
    ),
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Class',
      'handler' => 'views_handler_field_nodelink',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'og',
      'field' => 'description',
      'label' => 'Description',
    ),
    array (
      'tablename' => 'users',
      'field' => 'name',
      'label' => 'Submitted by',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => 'Date',
      'handler' => 'views_handler_field_date_small',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'class',
),
    ),
    array (
      'tablename' => 'twb_copy',
      'field' => 'copyable',
      'operator' => '=',
      'options' => '',
      'value' => 'copyable',
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(class_misc_predefined, node, og, users, twb_copy);
  $views[$view->name] = $view;


  return $views;

}

?>