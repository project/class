/**
 * Javascript support file for class
 */

// load a syllabus via AJAX from the server
function load_predefined_syllabus(url, id, target, s) {
	var recv = function(string, xmlhttp) {
		if(xmlhttp.status != 200)
		{
			return alert('An HTTP error ' + xmlhttp.status + ' occured.\n' + url);
		}
		
		s.selectedIndex = 0;
		target.value = string;
	}
	
	HTTPGet(url + '/' + id, recv);
}

// seek out any matching form elements and modify them to use AJAX
if (isJsEnabled()) {
  addLoadEvent(classClassAttachToSelectionBoxes);
}

function classClassMakeOnChangeFunction(s)
{
	return function() {
		id = s.options[s.selectedIndex].value;
		if(confirm('Are you sure you want to load this syllabus?  It will overwrite any changes you have made so far.')) {
			if(id != 0) {
				uri = s.getAttribute('uri');
				target = s.getAttribute('target');
				load_predefined_syllabus(uri, id, document.getElementById(target), s);
			}
		}
		else
		{
			s.selectedIndex = 0;
		}
	}
}

function classClassAttachToSelectionBoxes() {
  var selects = document.getElementsByTagName('select');
  for (var i = 0; s = selects[i]; i++) {
    if (s && hasClass(s, 'linked-selection')) {
      s.onchange = classClassMakeOnChangeFunction(s);
      
      button = document.getElementById(s.getAttribute('button'));
      
      button.parentNode.removeChild(button);
    }
  }
}